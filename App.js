import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import MainNavigation from './src/navigations/MainNavigation';


const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <MainNavigation />
  </NavigationContainer>
  )
}

export default App