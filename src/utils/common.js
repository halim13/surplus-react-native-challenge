export const ValidateEmail = (value) => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
        return ''
    }
    return 'Alamat Email Tidak Valid!'
}

export const ValidatePassword = (pass1, pass2) => {
    if (pass1 === pass2) {
        return ''
    }
    return 'Password tidak Sesuai!'
}
