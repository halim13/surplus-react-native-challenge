import React, { useState, useEffect } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Login from '../views/auth/Login'
import Register from '../views/auth/Register'
import Welcome from '../views/auth/Welcome'
import ForgotPassword from '../views/auth/ForgotPassword'
import HomeNavigation from './HomeNavigation'
import ListProduct from '../views/main/ListProduct'
import DetailProduct from '../views/main/DetailProduct'

const Stack = createStackNavigator()

const MainNavigation = () => {
    const [isLogIn, setIsLogIn] = useState(false)
    useEffect(() => {
        const checkLoggedIn = async () => {
            try {
                const value = await AsyncStorage.getItem('isLogIn')
                if (!!value) {
                    setIsLogIn(true)
                }
            } catch (e) {
                console.log('Error logIn', e)
            }
        }

        checkLoggedIn()

        return () => null
    }, [])

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}
        >
            {!isLogIn && (
                <>
                    <Stack.Screen name='Welcome' component={Welcome} />
                    <Stack.Screen name='Login' component={Login} />
                    <Stack.Screen name='Register' component={Register} />
                    <Stack.Screen name='ForgotPassword' component={ForgotPassword} />
                </>
            )}
            <Stack.Screen name='MainNav' component={HomeNavigation} />
            <Stack.Screen name='ListProduct' component={ListProduct} />
            <Stack.Screen name='DetailProduct' component={DetailProduct} />
        </Stack.Navigator>

    )
}

export default MainNavigation