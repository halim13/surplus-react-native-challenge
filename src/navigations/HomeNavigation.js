import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Home from '../views/main/Home'
import { IconButton } from 'react-native-paper'
import Order from '../views/main/Order'
import Forum from '../views/main/Forum'
import Profile from '../views/main/Profile'

const Tab = createBottomTabNavigator()

const HomeNavigation = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarActiveTintColor: '#009788',
            }}>
            <Tab.Screen
                name="DISCOVER"
                component={Home}
                options={{
                    tabBarIcon: ({ focused, color }) => <IconButton icon={'silverware-fork-knife'} iconColor={focused ? color : '#ddd'} />
                }}
            />
            <Tab.Screen
                name="PESANAN"
                component={Order}
                options={{
                    tabBarIcon: ({ focused, color }) => <IconButton icon={'shopping'} iconColor={focused ? color : '#ddd'} />
                }}
            />
            <Tab.Screen
                name="FORUM"
                component={Forum}
                options={{
                    tabBarIcon: ({ focused, color }) => <IconButton icon={'forum'} iconColor={focused ? color : '#ddd'} />
                }}
            />
            <Tab.Screen
                name="PROFIL"
                component={Profile}
                options={{
                    tabBarIcon: ({ focused, color }) => <IconButton icon={'human-male'} iconColor={focused ? color : '#ddd'} />
                }}
            />
        </Tab.Navigator>
    )
}

export default HomeNavigation