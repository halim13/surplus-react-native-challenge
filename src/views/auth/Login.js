import React, { useState } from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Button, Divider, Text, TextInput } from 'react-native-paper'
import AsyncStorage from '@react-native-async-storage/async-storage'

const DEFAULT_EMAIL = 'user@mail.com'
const DEFAULT_PASSWORD = '123456'

const Login = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [errorLogin, setErrorLogin] = useState('')
    const [password, setPassword] = useState('')
    const [togglePassword, setTogglePassword] = useState(false)

    const setInput = setter => val => {
        setter(val)
        setErrorLogin('')
    }

    const checkLogin = () => {
        if(!!email && !!password) {
            return false
        }
        return true
    }

    const onLogin = async() => {
        if(email === DEFAULT_EMAIL && password === DEFAULT_PASSWORD) {
            await AsyncStorage.setItem('isLogIn', 'true')
            navigation.navigate('MainNav')
        } else {
            setErrorLogin('Email atau Password tidak sesuai!')
        }
    }

    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/img/header.jpg')}
                style={styles.image}
            />
            <View style={{
                paddingHorizontal: 16,
                marginBottom: 16,
            }}>
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#fff', marginBottom: 8 }}>Masuk</Text>
                <Text style={{ color: '#fff', fontSize: 12 }}>Pastikan kamu sudah pernah membuat akun</Text>
            </View>
            <View style={styles.bottom}>
                <TextInput
                    value={email}
                    onChangeText={setInput(setEmail)}
                    mode='outlined'
                    error={errorLogin}
                    label='E-mail'
                    style={{ marginBottom: 16 }}
                />
                <TextInput
                    value={password}
                    onChangeText={setInput(setPassword)}
                    mode='outlined'
                    label='Kata Sandi'
                    error={errorLogin}
                    secureTextEntry={!togglePassword}
                    right={<TextInput.Icon icon={togglePassword ? 'eye-off' : 'eye'} onPress={() => setTogglePassword(!togglePassword)} />}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end'}}>
                    <Button labelStyle={{ color: 'grey' }} style={{ marginBottom: 16 }} onPress={() => navigation.navigate('ForgotPassword')}>Lupa kata sandi?</Button>
                </View>
                <Button mode='contained' disabled={checkLogin()} onPress={() => onLogin()}>Masuk</Button>
                {!!errorLogin && <Text style={{ marginBottom: 8, marginTop: 8, color: 'red' }}>{errorLogin}</Text>}
                <View style={{ flexDirection: 'row', marginVertical: 32, alignItems: 'center' }}>
                    <Divider style={{ flex: 1 }} />
                    <Text style={{ marginHorizontal: 16, color: 'grey' }}>Atau</Text>
                    <Divider style={{ flex: 1 }} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 32 }}>
                    <Button mode='contained' buttonColor='#e1e1e1' labelStyle={{ color: 'black' }} icon='facebook' style={{ marginRight: 16 }}>Facebook</Button>
                    <Button mode='contained' buttonColor='#e1e1e1' labelStyle={{ color: 'black' }} icon='google'>Google</Button>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Belum punya akun?</Text><Button onPress={() => navigation.navigate('Register')}>
                        Yuk daftar
                    </Button>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1e82c0',
    },
    image: {
        width: '100%',
        height: 150,
        resizeMode: 'cover',
    },
    bottom: {
        padding: 32,
        borderTopLeftRadius: 32,
        borderTopRightRadius: 32,
        backgroundColor: '#fff',
        flex: 1,
    },
    title: { fontWeight: 'bold', textAlign: 'center', fontSize: 20, },
    subTitle: { color: 'grey', textAlign: 'center', marginBottom: 32 },
    button: { marginBottom: 16 },
    privacy: { textAlign: 'center', fontSize: 12, color: 'grey' },
})


export default Login