import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Button, Text } from 'react-native-paper'

const Welcome = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/img/logo.jpg')}
                style={styles.image}
            />
            <View style={styles.bottom}>
                <Text style={styles.title}>Selamat datang di Chef's Place</Text>
                <Text style={styles.subTitle}>Selamatkan makanan berlebih di aplikasi Chef's Place agar tidak terbuang sia-sia</Text>
                <Button mode='contained' style={styles.button} onPress={() => navigation.navigate('Register')}>Daftar</Button>
                <Button mode='outlined' style={styles.button} onPress={() => navigation.navigate('Login')}>Sudah punya akun? Masuk</Button>
                <Text style={styles.privacy}>Dengan daftar atau masuk, Anda menrima <Text style={{ color: 'orange' }}>syarat dan ketentuan</Text> serta <Text style={{ color: 'orange' }}>kebijakan privasi</Text></Text>
            </View>
            <Button mode='outlined' onPress={() => navigation.navigate('MainNav')} style={{ position: 'absolute', top: 16, right: 16 }}>Lewati</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff1b0',
    },
    image: {
        width: '100%',
        height: 500,
        resizeMode: 'contain',
    },
    bottom: {
        padding: 32,
        borderTopLeftRadius: 32,
        borderTopRightRadius: 32,
        backgroundColor: '#fff',
        flex: 1,
    },
    title: { fontWeight: 'bold', textAlign: 'center', fontSize: 20, },
    subTitle: { color: 'grey', textAlign: 'center', marginBottom: 32 },
    button: { marginBottom: 16 },
    privacy: { textAlign: 'center', fontSize: 12, color: 'grey' },
})

export default Welcome
