import React, { useState } from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Button, Text, TextInput } from 'react-native-paper'

const DEFAULT_EMAIL = 'user@mail.com'

const ForgotPassword = () => {
    const [email, setEmail] = useState('')
    const [errorForgot, setErrorForgot] = useState('')

    const setInput = setter => val => {
        setter(val)
        setErrorForgot('')
    }

    const checkForgot = () => {
        if(!!email) {
            return false
        }
        return true
    }

    const onForgot = () => {
        if(email === DEFAULT_EMAIL) {
            alert('Silahkan cek email anda!')
        } else {
            setErrorForgot('Email tidak terdaftar!')
        }
    }

    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/img/forgot-password.jpg')}
                style={styles.image}
            />
            <View style={{
                paddingHorizontal: 16,
            }}>
                <Text style={{ fontWeight: 'bold', fontSize: 20, marginBottom: 8 }}>Lupa kata sandi?</Text>
                <Text style={{ fontSize: 12, color: 'grey' }}>Masukkan alamat email agar kita bantu kamu dalam memulihkan akun kamu</Text>
            </View>
            <View style={styles.bottom}>
                <TextInput
                    value={email}
                    onChangeText={setInput(setEmail)}
                    mode='outlined'
                    error={errorForgot}
                    label='E-mail'
                    style={{ marginBottom: 16 }}
                />
                <Button mode='contained' disabled={checkForgot()} onPress={() => onForgot()}>Kirim</Button>
                {!!errorForgot && <Text style={{ marginBottom: 8, marginTop: 8, color: 'red' }}>{errorForgot}</Text>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fefefe',
    },
    image: {
        width: '100%',
        height: 500,
        resizeMode: 'contain',
    },
    bottom: {
        padding: 16,
        flex: 1,
    },
})


export default ForgotPassword