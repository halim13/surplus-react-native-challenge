import React, { useState } from 'react'
import { View, Image, StyleSheet } from 'react-native'
import { Button, Divider, Text, TextInput } from 'react-native-paper'
import { ValidateEmail, ValidatePassword } from '../../utils/common'

const Register = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [errorEmail, setErrorEmail] = useState('')
    const [errorPassword, setErrorPassword] = useState('')
    const [password, setPassword] = useState('')
    const [togglePassword, setTogglePassword] = useState(false)
    const [rePassword, setRePassword] = useState('')
    const [toggleRePassword, setToggleRePassword] = useState(false)

    const setInput = setter => val => {
        setter(val)
    }

    const checkRegister = () => {
        if(!!email && !!password && !!rePassword && !errorEmail && !errorPassword) {
            return false
        }
        return true
    }

    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/img/header.jpg')}
                style={styles.image}
            />
            <View style={{
                paddingHorizontal: 16,
                marginBottom: 16,
            }}>
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#fff', marginBottom: 8 }}>Daftar</Text>
                <Text style={{ color: '#fff', fontSize: 12 }}>Lengkapi isian untuk mendaftar</Text>
            </View>
            <View style={styles.bottom}>
                <TextInput
                    value={email}
                    onChangeText={setInput(setEmail)}
                    onEndEditing={() => setErrorEmail(ValidateEmail(email))}
                    mode='outlined'
                    error={errorEmail}
                    label='E-mail'
                    style={{ marginBottom: errorEmail ? 0 : 16 }}
                />
                {!!errorEmail && <Text style={{ marginBottom: 8, marginTop: 8, color: 'red' }}>{errorEmail}</Text>}
                <TextInput
                    value={password}
                    onChangeText={setInput(setPassword)}
                    onEndEditing={() => setErrorPassword(rePassword ? ValidatePassword(password, rePassword) : '')}
                    mode='outlined'
                    label='Kata Sandi'
                    style={{ marginBottom: 16 }}
                    error={errorPassword}
                    secureTextEntry={!togglePassword}
                    right={<TextInput.Icon icon={togglePassword ? 'eye-off' : 'eye'} onPress={() => setTogglePassword(!togglePassword)} />}
                />
                <TextInput
                    value={rePassword}
                    onChangeText={setInput(setRePassword)}
                    onEndEditing={() => setErrorPassword(ValidatePassword(password, rePassword))}
                    mode='outlined'
                    label='Ulangi Kata Sandi'
                    style={{ marginBottom: errorPassword ? 0 : 16 }}
                    error={errorPassword}
                    secureTextEntry={!toggleRePassword}
                    right={<TextInput.Icon icon={toggleRePassword ? 'eye-off' : 'eye'} onPress={() => setToggleRePassword(!toggleRePassword)} />}
                />
                {!!errorPassword && <Text style={{ marginBottom: 8, marginTop: 8, color: 'red' }}>{errorPassword}</Text>}
                <Button mode='contained' disabled={checkRegister()} onPress={() => alert('Berhasil Daftar!')}>Daftar</Button>
                <View style={{ flexDirection: 'row', marginVertical: 32, alignItems: 'center' }}>
                    <Divider style={{ flex: 1 }} />
                    <Text style={{ marginHorizontal: 16, color: 'grey' }}>Atau</Text>
                    <Divider style={{ flex: 1 }} />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 32 }}>
                    <Button mode='contained' buttonColor='#e1e1e1' labelStyle={{ color: 'black' }} icon='google'>Google</Button>
                </View>
                <Text style={{ color: 'grey', textAlign: 'center', fontSize: 12 }}>Dengan mendaftar, Anda menerima <Text style={{ color: 'orange' }}>syarat dan ketentuan</Text> serta <Text style={{ color: 'orange' }}>kebijakan privasi</Text></Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text>Sudah punya akun?</Text><Button onPress={() => navigation.navigate('Login')}>
                        Yuk masuk
                    </Button>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1e82c0',
    },
    image: {
        width: '100%',
        height: 150,
        resizeMode: 'cover',
    },
    bottom: {
        padding: 32,
        borderTopLeftRadius: 32,
        borderTopRightRadius: 32,
        backgroundColor: '#fff',
        flex: 1,
    },
    title: { fontWeight: 'bold', textAlign: 'center', fontSize: 20, },
    subTitle: { color: 'grey', textAlign: 'center', marginBottom: 32 },
    button: { marginBottom: 16 },
    privacy: { textAlign: 'center', fontSize: 12, color: 'grey' },
})


export default Register