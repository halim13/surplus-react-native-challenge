import { StyleSheet, Text, View, Image } from 'react-native'
import React, { useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Appbar } from 'react-native-paper'

const Forum = ({ navigation }) => {
    useEffect(() => {
        const checkLogin = async () => {
            const isLogIn = await AsyncStorage.getItem('isLogIn')
            if (!isLogIn) {
                navigation.navigate('Welcome')
            }
        }
        checkLogin()
        return () => null
    }, [])

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Appbar.Header style={{ backgroundColor: '#0000' }}>
                <Appbar.Content title='Forum' style={{ alignItems: 'center' }} />
            </Appbar.Header>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <Image
                    source={require('../../assets/img/under-construction.png')}
                    style={{
                        width: '100%',
                        resizeMode: 'contain',
                    }}
                />
            </View>
        </View>
    )
}

export default Forum

const styles = StyleSheet.create({})