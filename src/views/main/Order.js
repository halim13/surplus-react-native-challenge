import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Appbar } from 'react-native-paper'

const Order = ({ navigation }) => {
    useEffect(() => {
        const checkLogin = async () => {
            const isLogIn = await AsyncStorage.getItem('isLogIn')
            if (!isLogIn) {
                navigation.navigate('Welcome')
            }
        }
        checkLogin()
        return () => null
    }, [])

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#fff',
        }}>
            <Appbar.Header style={{ backgroundColor: '#0000' }}>
                <Appbar.Content title='Daftar Pesanan' style={{ alignItems: 'center' }} />
            </Appbar.Header>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <Image
                    source={require('../../assets/img/no_order.png')}
                    style={{
                        width: '100%',
                        resizeMode: 'contain',
                    }}
                />
            </View>
        </View>
    )
}

export default Order

const styles = StyleSheet.create({})