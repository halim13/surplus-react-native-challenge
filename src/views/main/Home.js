import React, { useState, useEffect } from 'react'
import { View, ScrollView, RefreshControl, Image, FlatList, TouchableOpacity, Dimensions } from 'react-native'
import { Appbar, List, Searchbar, Text } from 'react-native-paper'
import { BASE_URL, requestGet } from '../../utils/requestUtils'
import { SliderBox } from "react-native-image-slider-box"

const images = [
    "https://source.unsplash.com/1024x768/?nature",
    "https://source.unsplash.com/1024x768/?water",
    "https://source.unsplash.com/1024x768/?girl",
    "https://source.unsplash.com/1024x768/?tree",
]

const Home = ({ navigation }) => {
    const [categories, setCategories] = useState([])
    const [statusData, setStatusData] = useState('idle')
    const [errorMessage, setErrorMessage] = useState('')
    const [query, setQuery] = useState('')

    const { width } = Dimensions.get('window')

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async refresh => {
        try {
            setStatusData('progress')
            const listCategories = await requestGet(BASE_URL + '/categories.php')
            setCategories(listCategories?.categories)
            setStatusData('ready')
        } catch (error) {
            console.log('error get categories', error)
            setStatusData('error')
            setErrorMessage(error.error)
        }
    }

    const renderItem = ({ item }) => {
        return <TouchableOpacity
            activeOpacity={0.7}
            style={{
                marginHorizontal: 8,
                backgroundColor: '#ddd',
                borderRadius: 12,
                paddingVertical: 4,
            }}
            onPress={() => navigation.navigate('ListProduct', { category: item?.strCategory })}
        >
            <Image
                source={{
                    uri: item.strCategoryThumb
                }}
                style={{
                    width: 150,
                    height: 150,

                }}
            />
            <Text style={{ textAlign: 'center' }}>{item?.strCategory}</Text>
        </TouchableOpacity>
    }

    return (
        <View style={{
            flex: 1,
        }}>
            <ScrollView style={{ flex: 1 }}
                refreshControl={<RefreshControl
                    refreshing={statusData === 'progress'}
                />}
            >
                {/* Header */}
                <Appbar.Header style={{ backgroundColor: 'rgb(0, 107, 95)' }}>
                    <Appbar.Content title={'Jl. gang senggol dong bang'} titleStyle={{ color: '#fff', fontSize: 14 }} />
                    <Appbar.Action icon={'heart-outline'} iconColor='#fff' />
                    <Appbar.Action icon={'cart-outline'} iconColor='#fff' />
                </Appbar.Header>
                <View style={{
                    backgroundColor: 'rgb(0, 107, 95)',
                    padding: 16,
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                    marginBottom: 30,
                }}>
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20 }}>Hi, User!</Text>
                    <Searchbar
                        value={query}
                        placeholder='Mau selamatkan makanan apa hari ini?'
                        placeholderTextColor={'grey'}
                        style={{
                            marginTop: 20,
                            marginBottom: -40,
                            borderRadius: 12,
                        }}
                    />
                </View>
                {/* Slider */}
                <View style={{
                    margin: 16,
                }}>
                    <SliderBox
                        images={images}
                        sliderBoxHeight={250}
                        autoplay
                        parentWidth={width - 32}
                        onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                    />
                </View>
                {/* Saldo */}
                <View style={{
                    backgroundColor: '#ddd',
                    borderRadius: 12,
                    margin: 16,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <List.Item
                        title='Rp0'
                        titleStyle={{ color: 'green', fontSize: 13, }}
                        description='FoodPay'
                        descriptionStyle={{ color: 'grey', fontSize: 12 }}
                        left={props => <List.Icon {...props} icon='currency-usd' />}
                        style={{ flex: 1 }}
                    />
                    <View style={{
                        borderWidth: 1,
                        height: '50%',
                        borderColor: 'grey',
                    }} />
                    <List.Item
                        title='2 voucher tersedia'
                        titleStyle={{ color: 'green', fontSize: 13 }}
                        left={props => <List.Icon {...props} icon='ticket' />}
                        style={{ flex: 1 }}
                    />
                </View>
                {/* kategori */}
                <FlatList
                    data={categories}
                    keyExtractor={(_, i) => i.toString()}
                    renderItem={renderItem}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                />
            </ScrollView>
        </View>
    )
}

export default Home