import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Appbar, Button } from 'react-native-paper'

const Profile = ({ navigation }) => {
    useEffect(() => {
        const checkLogin = async () => {
            const isLogIn = await AsyncStorage.getItem('isLogIn')
            if (!isLogIn) {
                navigation.navigate('Welcome')
            }
        }
        checkLogin()
        return () => null
    }, [])

    
    const logout = async () => {
        await AsyncStorage.removeItem('isLogIn')
        navigation.navigate('Welcome')
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Appbar.Header style={{ backgroundColor: '#0000' }}>
                <Appbar.Content title='Profile' style={{ alignItems: 'center' }} />
            </Appbar.Header>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <Button
                    onPress={() => logout()}
                    mode='contained'
                >logout</Button>
            </View>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({})