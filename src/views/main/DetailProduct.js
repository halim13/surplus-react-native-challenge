import React, { useState, useEffect } from 'react'
import {
    ScrollView,
    View,
    Image,
    Linking,
} from 'react-native'
import { Appbar, Button, Card, Text } from 'react-native-paper'
import { BASE_URL, requestGet } from '../../utils/requestUtils'

const TextInfo = ({ label, description }) => {
    return (
        <View style={{ marginBottom: 16 }}>
            {
                !!label && <Text style={{ fontWeight: 'bold', color: '#a1a1a1' }}>{label}</Text>
            }
            <Text>{description}</Text>
        </View>
    )
}

const DetailProduct = ({ navigation, route }) => {
    const { item } = route.params
    const [detail, setDetail] = useState(null)
    const [statusData, setStatusData] = useState('idle')

    useEffect(() => {
        getData()
        return () => null
    }, [])

    const getData = async () => {
        try {
            setStatusData('progress')
            const data = await requestGet(BASE_URL + '/lookup.php', { i: item?.idMeal })
            setDetail(data.meals[0])
            setStatusData('ready')
        } catch (error) {
            setStatusData('error')
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: '#0000' }}>
                <Appbar.BackAction onPress={() => navigation.goBack()} />
                <Appbar.Content title={item?.strMeal || 'detail'} style={{ alignItems: 'center' }} />
                <Appbar.Action />
            </Appbar.Header>
            <ScrollView
                style={{
                    flex: 1,
                }}
                contentContainerStyle={{
                    padding: 16,
                }}>
                <Card>
                    <View style={{ flexDirection: 'row', paddingVertical: 16 }}>
                        <View>
                            <Image
                                source={{
                                    uri: item?.strMealThumb
                                }}
                                style={{
                                    width: 80,
                                    height: 80,
                                    resizeMode: 'contain',
                                    backgroundColor: '#F8F8F8',
                                    marginHorizontal: 8,
                                    borderRadius: 8,
                                }}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontWeight: 'bold' }}>{item?.strMeal}</Text>
                            <Text style={{ color: '#a1a1a1' }}>{detail?.strCategory}</Text>
                            <Text style={{ color: '#a1a1a1' }}>{detail?.strArea}</Text>
                        </View>
                    </View>
                </Card>

                <Card style={{ marginTop: 16 }}>
                    <Card.Content>
                        <View
                            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TextInfo label={'Tags'} description={detail?.strTags || '-'} />
                            <View>
                                <Button
                                    mode='contained'
                                    onPress={() => Linking.openURL(detail?.strYoutube)}>
                                    Youtube
                                </Button>
                            </View>
                        </View>
                        <TextInfo label={'Instruction'} description={detail?.strInstructions} />
                        <Button mode='contained' onPress={() => Linking.openURL(detail?.strSource)}>
                            Source
                        </Button>
                    </Card.Content>
                </Card>
            </ScrollView>
        </View>
    )
}

export default DetailProduct