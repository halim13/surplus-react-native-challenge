import React, { useEffect, useState } from 'react'
import { Alert, FlatList, Image, RefreshControl, View } from 'react-native'
import {
    Appbar,
    Card,
    IconButton,
    Searchbar,
    Text,
} from 'react-native-paper'
import { BASE_URL, requestGet } from '../../utils/requestUtils'
import lodash from 'lodash'
import AsyncStorage from '@react-native-async-storage/async-storage'
// import ImageCompany from '../components/ImageCompany'

const ListProduct = ({ navigation, route }) => {
    const [statusData, setStatusData] = useState('idle')
    const [errorMessage, setErrorMessage] = useState('')
    const [data, setData] = useState([])
    const [originalData, setOriginalData] = useState([])
    const [query, setQuery] = useState('')

    const { category } = route.params

    const setInput = setter => val => {
        setter(val)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async refresh => {
        try {
            const params = {
                // s: query,
                c: category
            }

            setStatusData('progress')

            let result = []
            const response = await requestGet(BASE_URL + '/filter.php', params)
            if (!data || !!refresh) {
                setErrorMessage('')
                result = response?.meals
            } else {
                result = lodash.unionBy(data, response?.meals, 'idMeal')
            }
            setData(result)
            setOriginalData(result)
            setStatusData('ready')
        } catch (error) {
            setStatusData('error')
            setErrorMessage(error.error)
        }
    }

    filterData = () => {
        if(query) {
            setData(originalData.filter(ori => ori.strMeal.toLowerCase().includes(query.toLowerCase())))
        } else {
            setData(originalData)
        }
    }

    const renderItem = ({ item }) => {
        return (
            <View
                style={{
                    paddingHorizontal: 16,
                    paddingVertical: 8,
                }}>
                <Card onPress={() => navigation.navigate('DetailProduct', { item })}>
                    <View style={{ flexDirection: 'row', paddingVertical: 16 }}>
                        <View>
                            <Image
                                source={{
                                    uri: item?.strMealThumb
                                }}
                                style={{
                                    width: 80,
                                    height: 80,
                                    resizeMode: 'contain',
                                    backgroundColor: '#F8F8F8',
                                    marginHorizontal: 8,
                                    borderRadius: 8,
                                }}
                            />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Text style={{ fontWeight: 'bold' }}>{item?.strMeal}</Text>
                        </View>
                        <View style={{ justifyContent: 'center' }}>
                            <IconButton
                                icon='chevron-right'
                                size={30}
                                onPress={() => console.log('Pressed')}
                            />
                        </View>
                    </View>
                </Card>
            </View>
        )
    }

    const renderEmpty = () => !data?.length && statusData === 'ready' ? (
        <Text style={{ fontWeight: 'bold', textAlign: 'center', marginTop: 8 }}>
            Tidak ada Data Ditemukan!
        </Text>
    ) : <View />

    return (
        <View style={{ flex: 1 }}>
            <Appbar.Header style={{ backgroundColor: '#0000' }}>
                <Appbar.BackAction onPress={() => navigation.goBack()} />
                <Appbar.Content title={category || 'Daftar Produk'} style={{ alignItems: 'center' }} />
                <Appbar.Action />
            </Appbar.Header>
            <Card style={{ borderRadius: 0 }}>
                <Card.Content>
                    <Searchbar
                        placeholder='Search'
                        onChangeText={setInput(setQuery)}
                        onEndEditing={() => {
                            // fetchData(true)
                            filterData()
                        }}
                        value={query}
                        returnKeyType='go'
                    />
                </Card.Content>
            </Card>

            <FlatList
                showsVerticalScrollIndicator={false}
                data={data?.filter(val => !!val) || []}
                keyExtractor={(_, i) => i.toString()}
                renderItem={renderItem}
                onEndReachedThreshold={0.01}
                ListEmptyComponent={renderEmpty}
                refreshControl={
                    <RefreshControl
                        refreshing={statusData === 'progress'}
                        onRefresh={() => {
                            fetchData(true)
                        }}
                    />
                }
            />
        </View>
    )
}

export default ListProduct